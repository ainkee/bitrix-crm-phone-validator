<?php

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

class cntl_crmphonevalidator extends CModule
{
    var $MODULE_ID = 'cntl.crmphonevalidator';
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    public function __construct()
    {
        $arModuleVersion = [];
        include $this->getModulePath() . '/install/version.php';
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = Loc::getMessage('PhoneValidator.MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PhoneValidator.MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PhoneValidator.PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PhoneValidator.MODULE_PARTNER_URI');
        Loader::registerNamespace('CNTL\CrmPhoneValidator', $this->getModulePath() . '/lib/');
    }

    public function DoInstall(): void
    {
        Loader::includeModule('crm');
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallFiles();
        $this->InstallEvents();
        $this->InstallCustomField();
    }

    public function DoUninstall(): void
    {
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UnInstallCustomField();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    function InstallEvents(): void
    {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler(
            'main',
            'OnEpilog',
            $this->MODULE_ID,
            "CNTL\\CrmPhoneValidator\\Handler",
            'addResources'
        );
        $eventManager->registerEventHandler(
            'crm',
            'OnAfterCrmLeadAdd',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onLeadUpdate'
        );
        $eventManager->registerEventHandler(
            'crm',
            'OnAfterCrmLeadUpdate',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onLeadUpdate'
        );
        $eventManager->registerEventHandler(
            'crm',
            'OnAfterCrmDealAdd',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onDealUpdate'
        );
        $eventManager->registerEventHandler(
            'crm',
            'OnAfterCrmDealUpdate',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onDealUpdate'
        );

    }

    function UnInstallEvents(): void
    {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler(
            'main',
            'OnEpilog',
            $this->MODULE_ID,
            "CNTL\\CrmPhoneValidator\\Handler",
            'addResources'
        );
        $eventManager->unRegisterEventHandler(
            'crm',
            'OnAfterCrmLeadAdd',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onLeadUpdate'
        );
        $eventManager->unRegisterEventHandler(
            'crm',
            'OnAfterCrmLeadUpdate',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onLeadUpdate'
        );
        $eventManager->unRegisterEventHandler(
            'crm',
            'OnAfterCrmDealAdd',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onDealUpdate'
        );
        $eventManager->unRegisterEventHandler(
            'crm',
            'OnAfterCrmDealUpdate',
            $this->MODULE_ID,
            'CNTL\\CrmPhoneValidator\\PhoneValidationFieldUpdater',
            'onDealUpdate'
        );
    }

    function InstallFiles(): void
    {
        $documentRoot = Application::getDocumentRoot();

        \Bitrix\Main\IO\Directory::createDirectory(
            $documentRoot . '/local/js/' . $this->MODULE_ID
        );
        \Bitrix\Main\IO\Directory::createDirectory(
            $documentRoot . '/local/css/' . $this->MODULE_ID
        );
        CopyDirFiles(
            __DIR__ . '/files/js/',
            $documentRoot . '/local/js/' . $this->MODULE_ID,
            true,
            true
        );
        CopyDirFiles(
            __DIR__ . '/files/css/',
            $documentRoot . '/local/css/' . $this->MODULE_ID,
            true,
            true
        );
    }

    function UnInstallFiles(): void
    {
        DeleteDirFilesEx('/local/js/' . $this->MODULE_ID);
        DeleteDirFilesEx('/local/css/' . $this->MODULE_ID);
    }

    /**
     * @return string
     */
    protected function getModulePath(): string
    {
        return __DIR__ . '/..';
    }

    function InstallCustomField(): void
    {
        $fieldNames = ["UF_CRM_PHONE_VALID"];
        $entities = ['LEAD', 'DEAL'];

        $userType = new CUserTypeEntity();

        foreach ($entities as $entity) {
            foreach ($fieldNames as $fieldName) {
                $field = [
                    'ENTITY_ID' => 'CRM_' . $entity,
                    'FIELD_NAME' => $fieldName,
                    'USER_TYPE_ID' => 'enumeration',
                    'XML_ID' => $fieldName,
                    'SORT' => 100,
                    'MULTIPLE' => 'N',
                    'MANDATORY' => 'N',
                    'SHOW_FILTER' => 'Y',
                    'SHOW_IN_LIST' => 'Y',
                    'EDIT_IN_LIST' => 'N',
                    'IS_SEARCHABLE' => 'Y',
                    'EDIT_FORM_LABEL' => [
                        'ru' => 'Корректность телефона',
                        'en' => 'Phone valid',
                    ],
                    'LIST_COLUMN_LABEL' => [
                        'ru' => 'Корректность телефона',
                        'en' => 'Phone valid',
                    ],
                ];

                $userTypeId = $userType->Add($field);

                $enum = new CUserFieldEnum();
                $enumValues = [
                    'n0' => ['VALUE' => 'Да', 'DEF' => 'N', 'SORT' => '100'],
                ];
                $enum->SetEnumValues($userTypeId, $enumValues);
            }
        }
    }

    function UnInstallCustomField(): void
    {
        $fieldNames = ["UF_CRM_PHONE_VALID"];
        $entities = ['LEAD', 'DEAL'];

        foreach ($entities as $entity) {
            foreach ($fieldNames as $fieldName) {
                $rsData = CUserTypeEntity::GetList([], ['ENTITY_ID' => 'CRM_' . $entity, 'FIELD_NAME' => $fieldName]);
                if ($arRes = $rsData->Fetch()) {
                    $userTypeId = $arRes['ID'];
                    (new CUserTypeEntity)->Delete($userTypeId);
                }
            }
        }
    }
}
