const Constants = {
    API_URL: '/local/modules/cntl.crmphonevalidator/ajax.php',
    POLL_INTERVAL: 500,
    VALIDATION_ACTION: 'VALIDATE_PHONE',
    SUCCESS_ICON_CLASS: 'success-icon',
    ERROR_ICON_CLASS: 'error-icon',
    PHONE_INPUT_WRAPPER_SELECTORS: ['.crm-entity-widget-content-input-wrapper', 'div[data-cid=\'PHONE\'] .ui-entity-editor-content-block .ui-ctl-w100'],
    PHONE_INPUT_SELECTOR: '.crm-entity-widget-content-input-phone',
};

class PhoneValidator {
    static async validate(phone, countryCode) {
        try {
            const response = await fetch(Constants.API_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: new URLSearchParams({
                    ACTION: Constants.VALIDATION_ACTION,
                    PHONE: phone,
                    COUNTRY_CODE: countryCode
                })
            });
            const data = await response.json();
            return data.success ? { valid: true } : { valid: false, message: data.message };
        } catch (err) {
            console.error('API Error:', err);
            return { valid: false, message: 'API Error' };
        }
    }
}