class PhoneDOMHelper {
    static findPhoneInputWrapper(wrapperSelectors, phoneInputSelector) {
        let phoneInputWrappers = [];

        for (const wrapperSelector of wrapperSelectors) {
            const allWrappers = document.querySelectorAll(wrapperSelector);
            for (const wrapper of allWrappers) {
                if (wrapper.querySelector(phoneInputSelector)) {
                    phoneInputWrappers.push(wrapper);
                }
            }
        }

        return phoneInputWrappers;
    }

    static removePreviousIcons(wrapperElement, successClass, errorClass) {
        const prevSuccessIcon = wrapperElement.querySelector(`.${successClass}`);
        const prevErrorIcon = wrapperElement.querySelector(`.${errorClass}`);
        if (prevSuccessIcon) prevSuccessIcon.remove();
        if (prevErrorIcon) prevErrorIcon.remove();
    }

    static createValidationIcon(isValid, message = '', successClass, errorClass, wrapperType) {
        const icon = document.createElement('span');
        icon.className = isValid ? successClass : errorClass;
        if (wrapperType === 'firstType') {
            icon.className += ' first-type';
        } else {
            icon.className += ' second-type';
        }
        icon.textContent = isValid ? '✓' : '✖';
        if (!isValid) {
            icon.title = message;
        }
        return icon;
    }

    static determineWrapperType() {
        return document.querySelector('input[name^="PHONE"][type="hidden"]') ? 'firstType' : 'secondType';
    }

    static handleSaveButton(isValid) {
        const saveButton = document.querySelector('button.ui-btn.ui-btn-success');
        if (saveButton) {
            saveButton.disabled = !isValid;
        }

    }
}

class PhoneObserver {
    constructor() {
        this.lastPhoneValues = {};
        this.lastCountryCodes = {};
    }

    init() {
        this.pollingID = setInterval(() => this.processPhoneAndCountryCode(), Constants.POLL_INTERVAL);
    }

    async processPhoneAndCountryCode() {
        const phoneInputWrappers = PhoneDOMHelper.findPhoneInputWrapper(Constants.PHONE_INPUT_WRAPPER_SELECTORS, Constants.PHONE_INPUT_SELECTOR);

        if (!phoneInputWrappers.length) {
            return;
        }

        for (const [id, phoneInputWrapper] of phoneInputWrappers.entries()) {
            const hiddenPhoneInput = phoneInputWrapper.parentElement.querySelectorAll('input[type="hidden"]')[0] || document.querySelector('input[name^="PHONE"][type="hidden"]');
            const hiddenCountryCodeInput = phoneInputWrapper.parentElement.querySelectorAll('input[type="hidden"]')[1] || document.querySelector('input[name^="PHONE"][name$="[VALUE_COUNTRY_CODE]"]');
            if (hiddenPhoneInput && hiddenCountryCodeInput) {
                const newPhoneValue = hiddenPhoneInput.value;
                const newCountryCode = hiddenCountryCodeInput.value;

                if (newPhoneValue !== this.lastPhoneValues[id] || newCountryCode !== this.lastCountryCodes[id]) {
                    const validationResult = await PhoneValidator.validate(newPhoneValue, newCountryCode);
                    this.handleUIChanges(phoneInputWrapper, validationResult);
                    this.lastPhoneValues[id] = newPhoneValue;
                    this.lastCountryCodes[id] = newCountryCode;
                }
            }
        }
    }

    handleUIChanges(phoneInputWrapper, { valid, message }) {
        const wrapperType = PhoneDOMHelper.determineWrapperType();
        PhoneDOMHelper.removePreviousIcons(phoneInputWrapper, Constants.SUCCESS_ICON_CLASS, Constants.ERROR_ICON_CLASS);
        const icon = PhoneDOMHelper.createValidationIcon(valid, message, Constants.SUCCESS_ICON_CLASS, Constants.ERROR_ICON_CLASS, wrapperType);
        phoneInputWrapper.appendChild(icon);
        PhoneDOMHelper.handleSaveButton(valid);
    }
}

BX.ready(() => {
    const phoneObserver = new PhoneObserver();
    phoneObserver.init();
});