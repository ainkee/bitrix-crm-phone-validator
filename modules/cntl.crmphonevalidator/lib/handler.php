<?php

namespace CNTL\CrmPhoneValidator;

use Bitrix\Main\Page\Asset;

/**
 * Class Handler
 *
 * @package CNTL\CrmPhoneValidator
 */
class Handler
{
    private const MODULE_ID = 'cntl.crmphonevalidator';

    private const PHONE_TRACKER_URL_PATTERNS = [
        '#^/crm/contact/details/\d+/?#',
        '#^/crm/company/details/\d+/?#',
        '#^/crm/deal/details/\d+/?#',
        '#^/crm/lead/details/\d+/?#'
    ];

    public static function addResources(): void
    {
        global $APPLICATION, $USER;

        if ($USER->IsAuthorized()) {
            $uri = $APPLICATION->GetCurUri();
            $parsed_url = parse_url($uri);
            $path = $parsed_url['path'];

            if (self::shouldIncludeResource(self::PHONE_TRACKER_URL_PATTERNS, $path)) {
                self::includeResource('common.js');
                self::includeResource('phone_tracker.js');
                self::includeResource('styles.css', 'css');
            }
        }
    }

    private static function shouldIncludeResource(array $patterns, string $path): bool
    {
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $path)) {
                return true;
            }
        }
        return false;
    }

    private static function includeResource(string $resourceName, string $type = 'js'): void
    {
        $pathFormat = ($type === 'js') ? '/local/js/%s/%s?version=%s' : '/local/css/%s/%s?version=%s';
        $asset = Asset::getInstance();
        $method = ($type === 'js') ? 'addJs' : 'addCss';
        $asset->$method(sprintf($pathFormat, self::MODULE_ID, $resourceName, time()));
    }
}