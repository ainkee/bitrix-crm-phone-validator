<?php

namespace CNTL\CrmPhoneValidator;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Crm\FieldMultiTable;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use CUserFieldEnum;
use CUserTypeEntity;
use Bitrix\Main\Result;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Config\Option;

class PhoneValidationFieldUpdater
{

    private const VALIDATION_FIELD_ID = 'UF_CRM_PHONE_VALID';

    /**
     * Обновляет поле валидации телефона для лида или сделки.
     *
     * @param array $arFields Массив полей лида или сделки.
     * @param string $entityType Тип сущности (LEAD или DEAL).
     * @throws LoaderException
     */
    public static function updateEntityPhoneValidation(array $arFields, string $entityType): void
    {
        if (!Loader::includeModule('crm')) {
            return;
        }

        $elementId = $arFields['ID'];
        if ($elementId === null) {
            return;
        }

        $phone = self::getPhoneValue($entityType, $elementId);
        if ($phone === null) {
            return;
        }

        $validator = new PhoneValidator($phone);
        $result = $validator->validate();
        $checkResultValue = self::getValidationFieldValue($entityType, $result);
        $needUpdate = $arFields[self::VALIDATION_FIELD_ID] != $checkResultValue;

        if (!$needUpdate) {
            return;
        }

        self::updateEntityField($entityType, $elementId, $checkResultValue);
    }

    /**
     * Возвращает значение телефона для заданной сущности.
     *
     * @param string $entityType Тип сущности.
     * @param int $elementId ID элемента.
     * @return string|null
     */
    private static function getPhoneValue(string $entityType, int $elementId): ?string
    {
        $entityId = $entityType === 'DEAL' ? 'CONTACT' : $entityType;
        try {
            $phoneData = FieldMultiTable::getList([
                'filter' => [
                    'ENTITY_ID' => $entityId,
                    'ELEMENT_ID' => $elementId,
                    'TYPE_ID' => 'PHONE'
                ],
                'select' => ['VALUE']
            ])->fetch();
        } catch (ObjectPropertyException|ArgumentException|SystemException $e) {
        }

        return $phoneData['VALUE'] ?? null;
    }

    /**
     * Обновляет пользовательское поле валидации телефона.
     *
     * @param string $entityType Тип сущности.
     * @param int $elementId ID элемента.
     * @param mixed $value Значение для обновления.
     * @throws LoaderException
     */
    private static function updateEntityField(string $entityType, int $elementId, mixed $value): void
    {
        if (!Loader::includeModule('crm')) {
            return;
        }
        $siteUrl = Option::get('main', 'server_name', '');

        $data = [
            'ACTION' => 'UPDATE_ENTITY',
            'ENTITY_TYPE' => $entityType,
            'ENTITY_ID' => $elementId,
            'VALUE' => $value
        ];

        $http = new HttpClient();
        $http->setHeader('Content-Type', 'application/x-www-form-urlencoded');

        $url = 'https://' . $siteUrl . '/local/modules/cntl.crmphonevalidator/ajax.php';

        $queryData = http_build_query($data);
        $result = $http->post($url, $queryData);
    }

    /**
     * Возвращает значение поля валидации на основе результата проверки.
     *
     * @param string $entityType Тип сущности.
     * @param Result $result Результат валидации.
     * @return mixed
     */
    private static function getValidationFieldValue(string $entityType, Result $result): mixed
    {
        $rsData = CUserTypeEntity::GetList([], ['ENTITY_ID' => 'CRM_' . $entityType, 'FIELD_NAME' => self::VALIDATION_FIELD_ID]);
        if ($arRes = $rsData->Fetch()) {
            $userTypeId = $arRes['ID'];
        } else {
            return null;
        }

        $enumList = CUserFieldEnum::GetList([], ['USER_FIELD_NAME' => self::VALIDATION_FIELD_ID]);
        while ($enum = $enumList->GetNext()) {
            if ($enum['VALUE'] === 'Да' && $enum['USER_FIELD_ID'] === $userTypeId) {
                return $result->isSuccess() ? $enum['ID'] : null;
            }
        }

        return null;
    }

    /**
     * Обработчик события обновления лида.
     *
     * @param array $arFields Массив полей лида.
     * @throws LoaderException
     */
    public static function onLeadUpdate(array $arFields): void
    {
        self::updateEntityPhoneValidation($arFields, 'LEAD');
    }

    /**
     * Обработчик события обновления сделки.
     *
     * @param array $arFields Массив полей сделки.
     * @throws LoaderException
     */
    public static function onDealUpdate(array $arFields): void
    {
        self::updateEntityPhoneValidation($arFields, 'DEAL');
    }
}