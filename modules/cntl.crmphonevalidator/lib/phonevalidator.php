<?php

namespace CNTL\CrmPhoneValidator;

use Bitrix\Main\Result;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use InvalidArgumentException;
use RuntimeException;

/**
 * Класс для валидации телефонных номеров.
 */
class PhoneValidator
{
    private string $phone;
    private string $countryCode;
    private static array $rules = [];

    /**
     * Конструктор класса PhoneValidator.
     *
     * @param string $phone Телефонный номер.
     * @param string|null $countryCode Код страны.
     * @param string|null $rulesPath Путь к файлу с правилами валидации.
     *
     * @throws InvalidArgumentException Если телефон или код страны не являются строками.
     */
    public function __construct(string $phone, string $countryCode = null, string $rulesPath = null)
    {
        $this->phone = $phone;

        $this->loadRules($rulesPath ?? dirname(__DIR__) . '/resources/rules.json');

        if ($countryCode === null || $countryCode === '') {
            $this->countryCode = $this->detectCountryCode();
        } else {
            $this->countryCode = $countryCode;
        }
    }

    /**
     * Загружает правила валидации из JSON файла.
     *
     * @param string $path Путь к файлу с правилами.
     *
     * @throws RuntimeException Если не удается прочитать или декодировать файл с правилами.
     */
    private function loadRules(string $path): void
    {
        if (empty(self::$rules)) {
            $json = file_get_contents($path);
            if ($json === false) {
                throw new RuntimeException(Loc::getMessage('PhoneValidator.RULES_READ_ERROR', ['#path#' => $path]));
            }

            self::$rules = json_decode($json, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new RuntimeException(Loc::getMessage('PhoneValidator.JSON_DECODE_ERROR'));
            }
        }
    }

    public function detectCountryCode(): string
    {
        foreach (self::$rules as $countryCode => $rule) {
            $prefixLength = strlen($rule['prefix']);
            if (substr($this->phone, 0, $prefixLength) === $rule['prefix']) {
                return $countryCode;
            }
        }
        return 'XX';
    }

    /**
     * Валидирует телефонный номер на основе загруженных правил.
     *
     * @return Result Результат валидации.
     */
    public function validate(): Result
    {
        $result = new Result();

        if ($this->countryCode === 'XX') {
            $result->addError(new Error(Loc::getMessage('PhoneValidator.INVALID_COUNTRY_CODE')));
            return $result;
        }

        if (!isset(self::$rules[$this->countryCode])) {
            $result->addError(new Error(Loc::getMessage('PhoneValidator.INVALID_COUNTRY_CODE')));
            return $result;
        }

        $rule = self::$rules[$this->countryCode];
        $prefixLength = strlen($rule['prefix']);
        $totalLength = $prefixLength + $rule['length'];
        $totalAltLength = $prefixLength + $rule['altLength'];

        if (substr($this->phone, 0, $prefixLength) !== $rule['prefix']) {
            $result->addError(new Error(Loc::getMessage('PhoneValidator.INVALID_PREFIX')));
        }

        if (strlen($this->phone) !== $totalLength && strlen($this->phone) !== $totalAltLength) {
            $result->addError(new Error(Loc::getMessage('PhoneValidator.INVALID_LENGTH')));
        }

        return $result;
    }
}