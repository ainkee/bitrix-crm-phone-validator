<?php

namespace CNTL\CrmPhoneValidator;

use Bitrix\Main\LoaderException;
use CNTL\CrmPhoneValidator\PhoneValidationFieldUpdater;

class PhoneValidationAgent
{
    /**
     * @throws LoaderException
     */
    public static function processPhoneValidation($elementId, $entityType): string
    {
        PhoneValidationFieldUpdater::updateEntityPhoneValidation($elementId, $entityType);
        return "";
    }
}