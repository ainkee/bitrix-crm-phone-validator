<?php
$MESS['PhoneValidator.INVALID_INPUT_TYPE'] = "Телефон и код страны должны быть строками.";
$MESS['PhoneValidator.RULES_READ_ERROR'] = "Не удалось прочитать правила из #path#";
$MESS['PhoneValidator.JSON_DECODE_ERROR'] = "Ошибка декодирования JSON с правилами.";
$MESS['PhoneValidator.INVALID_COUNTRY_CODE'] = "Недопустимый код страны.";
$MESS['PhoneValidator.INVALID_PREFIX'] = "Недопустимый префикс.";
$MESS['PhoneValidator.INVALID_LENGTH'] = "Недопустимая длина номера.";