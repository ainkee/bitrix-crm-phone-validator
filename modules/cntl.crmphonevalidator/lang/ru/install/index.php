<?php
$MESS['PhoneValidator.MODULE_NAME'] = "CNTL: Валидатор телефонов по коду страны";
$MESS['PhoneValidator.MODULE_DESCRIPTION'] = "Модуль для валидации номеров телефонов в CRM в зависимости от кода страны: лиды, контакты, компании и сделки.";
$MESS['PhoneValidator.PARTNER_NAME'] = "ContinentalCRM";
$MESS['PhoneValidator.MODULE_PARTNER_URI'] = "https://cntl.kz";

$MESS['PhoneValidator.DO_MODULE_INSTALL'] = "Установка модуля";
$MESS['PhoneValidator.DO_MODULE_INSTALL_SUCCESS'] = "Модуль успешно установлен";
