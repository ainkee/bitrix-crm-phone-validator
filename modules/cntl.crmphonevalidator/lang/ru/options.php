<?php
$MESS['PhoneValidator.SETTINGS'] = "Настройки";
$MESS['PhoneValidator.SETTINGS_PARAMETERS'] = "Настройка параметров";
$MESS['PhoneValidator.NO_SETTINGS_REQUIRED'] = "Настройка модуля не требуется.";
$MESS['PhoneValidator.CANCEL'] = "Отменить";
$MESS['PhoneValidator.CANCEL_TITLE'] = "Отменить изменения";