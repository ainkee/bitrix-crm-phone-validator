<?php
$MESS['PhoneValidator.INVALID_INPUT_TYPE'] = "Phone and country code must be strings.";
$MESS['PhoneValidator.RULES_READ_ERROR'] = "Failed to read rules from #path#";
$MESS['PhoneValidator.JSON_DECODE_ERROR'] = "Failed to decode JSON rules.";
$MESS['PhoneValidator.INVALID_COUNTRY_CODE'] = "Invalid country code.";
$MESS['PhoneValidator.INVALID_PREFIX'] = "Invalid prefix.";
$MESS['PhoneValidator.INVALID_LENGTH'] = "Invalid length.";