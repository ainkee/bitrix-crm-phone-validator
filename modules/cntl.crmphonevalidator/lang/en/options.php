<?php
$MESS['PhoneValidator.SETTINGS'] = "Settings";
$MESS['PhoneValidator.SETTINGS_PARAMETERS'] = "Settings Parameters";
$MESS['PhoneValidator.NO_SETTINGS_REQUIRED'] = "No settings required for this module.";
$MESS['PhoneValidator.CANCEL'] = "Cancel";
$MESS['PhoneValidator.CANCEL_TITLE'] = "Cancel changes";