<?php
$MESS['PhoneValidator.MODULE_NAME'] = "CNTL: Country Code Phone Validator";
$MESS['PhoneValidator.MODULE_DESCRIPTION'] = "Module for phone number validation in CRM based on country code: leads, contacts, companies, and deals.";
$MESS['PhoneValidator.PARTNER_NAME'] = "ContinentalCRM";
$MESS['PhoneValidator.MODULE_PARTNER_URI'] = "https://cntl.kz";

$MESS['PhoneValidator.DO_MODULE_INSTALL'] = "Installing module";
$MESS['PhoneValidator.DO_MODULE_INSTALL_SUCCESS'] = "Module successfully installed";