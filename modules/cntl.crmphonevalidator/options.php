<?php
/**
 * @var string $mid - module id from $_GET
 */
global $USER;
global $APPLICATION;
global $Update;

use Bitrix\Main\Localization\Loc;

if (!$USER->IsAdmin()) {
    return;
}

CModule::IncludeModule('crm');
CModule::IncludeModule('cntl.crmphonevalidator');

Loc::loadMessages(__FILE__);

$aTabs = [
    ["DIV" => "edit1", "TAB" => Loc::getMessage('PhoneValidator.SETTINGS'), "TITLE" => Loc::getMessage('PhoneValidator.SETTINGS_PARAMETERS')],
];
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$is_post = $_SERVER['REQUEST_METHOD'] === 'POST';
if ($is_post) {

    if ($Update <> '' && $_REQUEST["back_url_settings"] <> '') {
        LocalRedirect($_REQUEST["back_url_settings"]);
    } else {
        LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
    }
}

?>

<form method="post" action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= $mid ?>">
    <?php echo bitrix_sessid_post() ?>
    <?php
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <tr class="heading">
        <td colspan="2"><strong><?= Loc::getMessage('PhoneValidator.SETTINGS') ?></strong></td>
    </tr>
    <tr>
        <td colspan="2"><?= Loc::getMessage('PhoneValidator.NO_SETTINGS_REQUIRED') ?></td>
    </tr>

    <?php $tabControl->Buttons(); ?>
    <?php if ($_REQUEST["back_url_settings"] <> ''): ?>
        <input type="button" name="Cancel" value="<?= Loc::getMessage('PhoneValidator.CANCEL') ?>"
               title="<?= Loc::getMessage('PhoneValidator.CANCEL_TITLE') ?>"
               onclick="window.location='<?= htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
        <input type="hidden" name="back_url_settings" value="<?= htmlspecialcharsbx($_REQUEST["back_url_settings"]) ?>">
    <?php endif ?>
    <?php $tabControl->End() ?>
</form>