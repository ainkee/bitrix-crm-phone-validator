<?php

define("NO_KEEP_STATISTIC", true);
define("STOP_STATISTICS", true);
define("NO_AGENT_STATISTIC", "Y");
define("NOT_CHECK_PERMISSIONS", true);
define("BX_NO_ACCELERATOR_RESET", true);
define("DisableEventsCheck", true);
define("NO_AGENT_CHECK", true);

@set_time_limit(0);
@ignore_user_abort(true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
use Bitrix\Main\LoaderException;
use CNTL\CrmPhoneValidator\PhoneValidator;
use Bitrix\Main\Loader;

try {
    Loader::includeModule('cntl.crmphonevalidator');
    Loader::includeModule('crm');
} catch (LoaderException $e) {
}

$request = Application::getInstance()->getContext()->getRequest();

$action = $request->getPost("ACTION");

$response = [
    'success' => false,
    'message' => ''
];

if ($action === 'VALIDATE_PHONE') {
    $phone = $request->getPost("PHONE");
    $countryCode = $request->getPost("COUNTRY_CODE");

    $validator = new PhoneValidator($phone, $countryCode);
    $result = $validator->validate();

    if ($result->isSuccess()) {
        $response['success'] = true;
    } else {
        $errors = $result->getErrorMessages();
        $response['message'] = implode('; ', $errors);
    }
}

if ($action === 'UPDATE_ENTITY') {
    $entityType = $request->getPost("ENTITY_TYPE");
    $entityId = $request->getPost("ENTITY_ID");
    $value = $request->getPost("VALUE");

    $entityClass = $entityType === 'DEAL' ? new \CCrmDeal(false) : new \CCrmLead(false);
    $arFields = ['UF_CRM_PHONE_VALID' => $value ? (int)$value : ''];
    $entityClass->Update($entityId, $arFields, true, true, [
        'CURRENT_USER' => 1
    ]);

    $response['success'] = true;
}

header('Content-Type: application/json');
echo json_encode($response);